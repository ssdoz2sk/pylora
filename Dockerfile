FROM python:3.8-slim

WORKDIR sim

ENV USING_DOCKER=1

RUN pip install numpy tqdm

COPY . /sim

CMD ["python", "main.py"]