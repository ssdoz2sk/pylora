from functools import reduce
from itertools import tee

import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.animation import FuncAnimation

class LteAN:
    """Class that draw network animate"""
    
    def __init__(self, width, height):
        self.width = width 
        self.height = height
        self.fig, self.ax = plt.subplots()
        
        self.bs = []
        self.ue = []
        self._steps = None
        self._step_size = None
        self._bs_plot = None
        self._ue_plot = None


    def set_bs(self, bs):
        self.bs = bs
        return self

    def set_ue(self, ue):
        self.ue = ue
        return self

    def _draw_bs(self):
        bs_x = []
        bs_y = []
        for bs in self.bs:
            bs_x.append(bs['x'])
            bs_y.append(bs['y'])

        self._bs_plot, = self.ax.plot(bs_x, bs_y, 'r^', color="red", markersize = 10, animated=False)

    def _draw_bs_sinr(self):
        pass

    def _draw_ue(self, step):
        ue_x = []
        ue_y = []
        if step > len(self.steps):
            return 
        for ue in self.ue:
            if 'track' not in ue:
                ue_x.append(ue['x'])
                ue_y.append(ue['y'])
                continue
            ue_x.append(ue['track'][step]['x'])
            ue_y.append(ue['track'][step]['y'])
    
        if self._ue_plot:
            self._ue_plot.set_data(ue_x, ue_y)
        else:
            self._ue_plot, =  self.ax.plot(ue_x, ue_y, 'b*', color="black", markersize = 10, animated=False)

    def _draw_link_bs_ue(self, step):
        for ue in self.ue:
            if 'track' in ue and 'serving_bs' in ue['track'][step].keys():
                track = ue['track'][step]
                bsId = track['serving_bs']['id']
                bs = None
                for b in self.bs:
                    if b['id'] == bsId:
                        bs = b
                        break

                if 'link_serving_bs_ue' not in ue.keys():
                    line = Line2D([track['x'], bs['x']], 
                        [track['y'], bs['y']], 
                        linewidth=1, linestyle = "-", color="green")
                    ue['link_serving_bs_ue'] = line
                    self.ax.add_line(line)
                else:
                    ue['link_serving_bs_ue'].set_data(
                        [track['x'], bs['x']], 
                        [track['y'], bs['y']]
                    )
    
            # if 'target_bs' in ue['track'][step].keys():
            #     track = ue['track'][step]
            #     bsId = track['target_bs']['id']
            #     bs = None
            #     for b in self.bs:
            #         if b['id'] == bsId:
            #             bs = b
            #             break

            #     if 'link_target_bs_ue' not in ue.keys():
            #         line = Line2D([track['x'], bs['x']], 
            #             [track['y'], bs['y']], 
            #             linewidth=1, linestyle = "--", color="red")
            #         ue['link_target_bs_ue'] = line
            #         self.ax.add_line(line)
            #     else:
            #         ue['link_target_bs_ue'].set_data(
            #             [track['x'], bs['x']], 
            #             [track['y'], bs['y']]
            #         )

    def _draw_border(self):
        border = plt.Rectangle((0, 0), self.width, self.height, color="black", fill=False)
        self.ax.add_patch(border)

    def _calc_step(self):
        time_set = set()
        for u in self.ue:
            if 'track' not in u:
                continue
            for t in u['track']:
                time_set.add(t['t'])

        time_list = [float(t) for t in time_set]
        time_list.sort()
        if len(time_list) == 0:
            self._step_size = 0
            self._steps = []
            return 
            
        min_step_size = time_list[-1]
        def pairwize(iterable):
            a, b = tee(iterable)
            next(b, None)
            return zip(a, b)

        for t1, t2 in pairwize(time_list):
            if t2 - t1 < min_step_size:
                min_step_size = t2 - t1

        self._step_size = min_step_size
        self._steps = time_list

    def _step_to_time(self, step):
        if step < 0:
            return self._step[0]
            
        if step > len(self._steps):
            return self._steps[-1]

        return self._steps[step]

    @property
    def steps(self):
        if not self._steps:
            self._calc_step()

        return self._steps
    
    @property
    def step_size(self):
        if not self._step_size:
            self._calc_step()
            
        return self._step_size

    def draw(self, step, isAnimation=False):
        self._draw_bs()
        # self._draw_bs_sinr()
        self._draw_ue(step)
        self._draw_link_bs_ue(step)
        self._draw_border()
        self.ax.grid()
        self.ax.axis('equal')
        self.ax.axis([0, self.width, 0, self.height])
        
        if not isAnimation:
            plt.show()

    def animation_draw(self, step):
        self._draw_ue(step*100)
        self._draw_link_bs_ue(step*100)

        return self._ue_plot
        # self._draw_link_bs_ue()

    def play(self):
        self.draw(0, True)
        ani = FuncAnimation(fig=self.fig,
                              func=self.animation_draw,
                              frames=100,
                              interval=1,
                              blit=False)

        plt.show()
