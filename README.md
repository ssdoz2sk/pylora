# pyLoRa Sim

pyLoRa is a simulator which use to test collision avoidance algorithm. You can write your algorithm and import it in network to use it.

## 1. Requirements

* Python >= 3.7
* matplotlib
* numpy
* tqdm

## 2. How to use

```
usage: main.py [-h] 
               [--times TIMES] 
               [--min_node MIN_NODE] 
               [--max_node MAX_NODE] 
               [--min_period MIN_PERIOD]
               [--max_period MAX_PERIOD] 
               [--dir_name DIR_NAME]
               [--mode [MODE [MODE ...]]] 
               [--config CONFIG] 
               [--battery_life BATTERY_LIFE] 
               [--packet_length PACKET_LENGTH]

Run LoRa Sim

optional arguments:
  -h, --help            show this help message and exit
  --times TIMES         how much time sim run
  --min_node MIN_NODE   how much node in sim
  --max_node MAX_NODE   how much node in sim
  --min_period MIN_PERIOD
                        how much period in sim
  --max_period MAX_PERIOD
                        how much period in sim
  --dir_name DIR_NAME   where to save data
  --mode [MODE [MODE ...]]
  --config CONFIG       none, aggregate or append
  --battery_life BATTERY_LIFE
                        存活能量（瓦w）
  --packet_length PACKET_LENGTH
                        -1: 溫度資料, 0: 正常隨機資料 5-255bytes, 1: 5-100bytes, 2: 101-200bytes, 3: 201-255bytes
```

### 2.1 參數說明
* --times TIMES
    * 模擬次數設定
    * 預設為 1
* --min_node MIN_NODE
    * 最少的節點數量
    * 預設為 50
    * 節點數量會跟據此值逐項增加100，直到大於最大節點數量
* --max_node MAX_NODE
    * 最大節點數量
    * 預設為 2000
* --min_period MIN_PERIOD
    * 最低的發送間隔時間
    * 預設為 50
    * 發送間隔時間會逐漸增加，直到大於最大間隔時間
* --max_period MAX_PERIOD
    * 最高的發送間隔時間
    * 預設為 1000
* --dir_name DIR_NAME
    * 模擬資料存放位置
    * 預設為 執行路徑的 data 資料夾下
* --mode [MODE [MODE ...]]
    * 模擬的演算法，中間使用空格隔開
    * 預設為 ALOHA, NEW_LORA, BEH, FB
* --config CONFIG
    * none, aggregate or append
    * 是否啟用 aggregate 或 append
    * 預設為 none
* --battery_life BATTERY_LIFE
    * 存活時間
    * 存活能量（毫瓦mw）
* --packet_length PACKET_LENGTH
    * 封包長度設定
    * 預設為 -1
    * -1: 溫度資料
    * 0: 正常隨機資料 5-255bytes
    * 1: 5-100bytes
    * 2: 101-200bytes
    * 3: 201-255bytes

### 2.2 環境變數說明
* mode
    * 演算法使用的費氏數列項次
    * 演算法使用 費氏數列後退(FB) 時有效，預設為2
    * 1. 整數倍數 5~11 項後退
    * 2. 浮點數倍數 5~11 項後退
    * 3. 整數倍數 3~11 項後退
    * 4. 浮點數倍數 3~11 項後退
    * 5. 整數倍數 4~11 項後退
    * 6. 浮點數倍數 4~11 項後退
    * 7. 整數倍數 5~12 項後退
    * 8. 浮點數倍數 5~12 項後退
    * 9. 整數倍數 6~11 項後退
    * 10. 浮點數倍數 6~11 項後退
    * 11. 整數倍數 6~12 項後退
    * 12. 浮點數倍數 6~12 項後退
    * 101. 整數倍數 5~11 項後退
    * 102. 浮點數倍數 5~11 項後退
    * 103. 整數倍數 3~11 項後退
    * 104. 浮點數倍數 3~11 項後退
    * 105. 整數倍數 4~11 項後退
* CAD_T
    * 連續檢測頻道空閒的次數
    * 預設為 2
    * 演算法使用 費氏數列後退(FB)、累積確認(FB_COMULATIVE)、費氏數列後退2(FB2) 時有效
* CAD_CT
    * 累積確認的次數
    * 預設為 2
    * 演算法使用 累積確認(FB_COMULATIVE) 時有效
* RANDOM_MAX
    * 固定最大隨機後退時間
    * 預設為 max_toa
    * 演算法使用 固定後退時間(FIXED) 時有效

### 2.3 執行範例
1. 執行最基本的 ALOHA
```
    python main.py --mode ALOHA --dir_name data/aloha
```
2. 執行費氏數列後退演算法，並使用 aggregate
```
    python main.py --mode FB --dir_name data/FB_aggregate
```
3. 執行費氏數列後退演算法，並使用「整數倍數5~12項」的項次
```
    mode=7 python main.py --mode FB --dir_name data/FB_
```
4. 執行費氏數列後退演算法，並連續頻道偵測3次是空閒的
```
    CAD_T=3 python main.py --mode FB --dir_name data/FB_aggregate
```

## 3. docker 支援
Registry: https://gitlab.com/ssdoz2sk/pylora/container_registry/

或是使用 docker build 自己建立

## 4. docker-compose 支援
能快速建立多個實驗，請注意要把 data 資料夾匯出到外面的資料夾

## 5. 資料讀取
```
    python data_reader.py --dir_name data_dir > ratio.csv
```

## 6. 匯入 iPynb 讀取
請見 data_simple/ 內的範例