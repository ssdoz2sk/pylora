import argparse
import os


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Read LoRa Sim')
    parser.add_argument('--dir_name', type=str, default='data', help='where the data save')

    args = parser.parse_args()
    dir_name = args.dir_name

    print(f"mode,sim,nodes,period,c_packet_rate,c_sensor_want_send_packets,c_sensor_buffer_packets,c_sensor_send_success,c_sensor_drop_packets,agg_packets,append_packets,out_of_date_packets")

    for root, dirs, files in os.walk(dir_name):
        for f in files:
            if not f.endswith('data'):
                continue
            
            fullpath = os.path.join(root, f)
            c_packet_rate = ""
            mode = f.split('-')[0]
            nodes = f.split('-')[1].split('.')[0]
            period = root.split('/')[-2]
            sim = root.split('/')[-1]
            
            with open(fullpath, 'r') as fp:
                send_success = 0
                buffer_packets = 0
                want_send_packets = 0
                drop_packets = 0
                agg = 0
                append = 0
                out_of_date = 0

                for l in fp.readlines():
                    if l.startswith('c_packet'):
                        c_packet_rate = l.split(',')[-1].strip()

                    if l.startswith('sensor'):
                        d = l.split(',')
                        want_send_packets += int(d[2])
                        buffer_packets += int(d[3])
                        send_success += int(d[4])
                        drop_packets += int(d[5])

                        if len(d) >= 8:
                            agg += int(d[7])
                        if len(d) >= 9:
                            append += int(d[8])
                        if len(d) >= 10:
                            out_of_date += int(d[9])
                        
            print((f"{mode}, {sim}, {nodes}, {period}, {c_packet_rate}, {want_send_packets}, "
                   f"{buffer_packets}, {send_success}, {drop_packets}, {agg}, {append}, {out_of_date}"))
