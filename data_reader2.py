import argparse
import os
import json


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Read LoRa Sim')
    parser.add_argument('--dir_name', type=str, default='data', help='where the data save')

    args = parser.parse_args()
    dir_name = args.dir_name

    # print(f"mode,sim,backoff")

    backoff_sum = 0
    backoff_counter = 0

    for root, dirs, files in os.walk(dir_name):
        for f in files:
            if not f.endswith('json'):
                continue
            fullpath = os.path.join(root, f)
            mode = f.split('-')[0]
            sim = root.split('/')[-1]


            s1 = 0
            s2 = 0
            
            with open(fullpath, 'r') as fp:
                d = json.loads(fp.read())
                

                for key, value in d.items():
                    s1 += int(key) * value
                    s2 += value

            backoff_counter += 1
            backoff_sum += s1/s2
    
    print(backoff_sum/backoff_counter)
