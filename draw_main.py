
from LteAN import LteAN
import numpy as np

width = 1000
heigh = 1000
nodes = 500

if __name__ == '__main__':
    ltean = LteAN(width, heigh)
    ltean.set_bs([{'id': 0, 'x': 500, 'y': 500}])

    ue = []
    for n in range(nodes):
        x = np.random.randint(0, width)
        y = np.random.randint(0, heigh)
        ue.append({'id': n, 'x': x, 'y': y})

    ltean.set_ue(ue)

    ltean.draw(0)