from pyLoRa.network import Network, NetworkSetting
from tqdm import tqdm

import numpy as np
import argparse
import os

def sim(mode, dumpFilename, node=100, end_time=3600, time_step=0.1, period=100, options={}):
    # 場域大小
    heigh = 3000
    width = 3000

    # 設定網路參數
    networkSetting = NetworkSetting(code_rate=4)

    # 設定模擬跑多久、使用哪種演算法進行碰撞避免、資料匯出檔名、發送頻率等等
    network = Network(end_time=end_time, 
        mode=mode, dumpFileName=dumpFilename,
        time_step=time_step, period=period, options=options,
        networkSetting=networkSetting)

    # 設定場域大小
    network.setRange(width, heigh)
    network.addGateway(width / 2, heigh / 2, options=options)
    
    # 設定節點位置
    for n in range(node):
        x = np.random.randint(0, width)
        y = np.random.randint(0, heigh)
        sensor = network.addSensor(x, y, options=options)
        sensor.time_to_send_next_packet = np.random.uniform(0, 100) #period)

    # 使用 docker 不顯示進度條
    if not os.environ.get('USING_DOCKER'):
        progress_bar = tqdm(total=end_time / time_step)

    # 進行實驗模擬
    process = network.run()
    while True:
        try:
            # 進行下一個 tick 的模擬
            t = next(process)
            if not os.environ.get('USING_DOCKER'):
                progress_bar.update(1)
        except StopIteration:
            break

if __name__ == '__main__':
    modes = []

    parser = argparse.ArgumentParser(description='Run LoRa Sim')
    parser.add_argument('--times', type=int, default=1, help='how much time sim run')
    parser.add_argument('--min_node', type=int, default=100, help='how much node in sim')
    parser.add_argument('--max_node', type=int, default=2000, help='how much node in sim')
    parser.add_argument('--min_period', type=int, default=50, help='how much period in sim')
    parser.add_argument('--max_period', type=int, default=1000, help='how much period in sim')
    parser.add_argument('--dir_name', type=str, default='data', help='where to save data')
    parser.add_argument('--mode', type=str, nargs='*', default=['ALOHA', 'NEW_LORA', 'BEH', 'FB'])
    parser.add_argument('--config', type=str, default='', help='none, aggregate or append')
    parser.add_argument('--battery_life', type=int, default=None, help='存活能量（瓦w）')
    parser.add_argument('--packet_length', type=int, default=-1, help='-1: 溫度資料, 0: 正常隨機資料 5-255bytes, 1: 5-100bytes, 2: 101-200bytes, 3: 201-255bytes')
    
    args = parser.parse_args()
    min_node = args.min_node        # 最少節點數量
    max_node = args.max_node        # 最大節點數量
    min_period = args.min_period    # 最低發送等待時間
    max_period = args.max_period    # 最高發送等待時間
    dir_name = args.dir_name        # 資料儲存資料夾
    times = args.times              # 模擬次數
    battery_life = args.battery_life    # 存活時間
    packet_length = args.packet_length  # 封包長度參數

    options = {}

    print(args.mode)        # 碰撞避免演算法，可以指定多個
    if 'ALOHA' in args.mode:
        modes.append({'mode': Network.MODE_ALOHA, 'name': 'ALOHA'})

    if 'NEW_LORA' in args.mode:
        modes.append({'mode': Network.MODE_CSMA_NEW_LORA, 'name': 'NEW_LORA'})

    if 'BEH' in args.mode:
        modes.append({'mode': Network.MODE_CSMA_BEH, 'name': 'BEH'})

    if 'FB' in args.mode:
        modes.append({'mode': Network.MODE_CSMA_FB, 'name': 'FB'})

    if 'FB2' in args.mode:
        modes.append({'mode': Network.MODE_CSMA_FB2, 'name': 'FB2'})

    if 'FIXED' in args.mode:
        modes.append({'mode': Network.MODE_CSMA_FIXED, 'name': 'FIXED'})

    if 'FB_COMULATIVE' in args.mode:
        modes.append({'mode': Network.MODE_CSMA_FB_COMULATIVE, 'name': 'FB_COMULATIVE'})

    if 'append' == args.config:
        options['append'] = True
    if 'aggregate' == args.config:
        options['aggregate'] = True

    if battery_life:
        options['battery_life'] = battery_life
    if packet_length:
        options['packet_length'] = packet_length
    
    for t in range(times):
        for m in modes:
            for node in range(min_node, max_node+1, 100):
                for period in range(min_period, max_period+1, 50):

                    # 建立輸出資料夾
                    output_dirname = os.path.join(dir_name, m['name'], str(period), f"sim-{t + 1}")
                    output_filename = os.path.join(output_dirname, f"{m['name']}-{node}.data")

                    if not os.path.exists(output_dirname):
                        os.makedirs(output_dirname)
                    
                    # 建立模擬
                    sim(m['mode'], output_filename, node, time_step=0.01, period=period, options=options)
