from pyLoRa.packet import Packet
import numpy as np

class CSMA_BEH:
    def __init__(self, network):
        self.status = ()
        self.network = network
        self.max_failure = network.max_failure
        self.cad_base_counter = 9
        self.max_toa = Packet(None, 'a' * 255).setFromNetwork(network).toa # 最大封包 255

        print(f'max_toa: {self.max_toa}')

    def sensor_run(self, sendPacket):
        failure_counter = 0
        delay_until = 0

        startFlag = True
        fl = False
        i = 1
        i_max = 3
        k = 0

        while True:
            # 後退或 delay 時間還沒到
            if delay_until > self.network.t_now:
                yield 'delay'
                continue

            # 執行 CAD，CAD執行尚未結束會yield 'not done'
            if startFlag:
                startFlag = False
                sendPacket.source.startCAD()
                sendPacket.cad_counter += 1
                yield 'not done'

            status = sendPacket.source.checkCAD()
            # 如果發現碰撞
            if status == -1:
                failure_counter += 1
                startFalg = True
                d_cad = self.max_toa / 2 ** i
                delay_until = self.network.t_now + d_cad

                while True:
                    # 會 backoff delay
                    if delay_until > self.network.t_now:
                        yield 'backoff delay'
                    else:
                        i = i_max if i >= i_max else i + 1
                        fl = False
                        break
            # CAD執行尚未結束會yield 'not done'
            elif status == 0:
                yield 'not done'
            else:
                # 沒發現碰撞
                startFlag = True
                if fl:
                    yield 'success'
                else:
                    t_rb = np.random.randint(low=0, high=2**k+1)
                    t_rb *= self.max_toa
                    k += 1
                    delay_until = self.network.t_now + t_rb

                    while True:
                        # delay 時間還沒到
                        if delay_until > self.network.t_now:
                            yield 'delay'
                        else:
                            fl = True
                            i = 1
                            break

                

    def gateway_run(self, gateway):
        # gateway 接收 判斷碰撞
        gateway.checkReciveAllData()

        yield 'pass'
        return
    