from pyLoRa.packet import Packet
import numpy as np
import os

class CSMA_FB:
    def __init__(self, network):
        self.status = ()
        self.network = network
        self.max_failure = network.max_failure
        self.cad_base_counter = 9
        self.max_toa = Packet(None, 'a' * 255).setFromNetwork(network).toa # 最大封包 255
        self.backoff_counter = {}
        
        print(f'max_toa: {self.max_toa}')

    def sensor_run(self, sendPacket):
        failure_counter = 0
        delay_until = 0

        startFlag = True
        CAD_success_counter = 0
        rand_int = False
        # first_CAD_success = False
        # 去掉 [1, 2, 3] 原因：範圍太小，碰撞機率太大
        # 各種模式的費氏數列
        if os.environ.get('mode') == '1':                   # 整數倍數 5~11 項後退
            rand_int = True
            f_list = [5, 8, 13, 21, 34, 55, 89]
        elif os.environ.get('mode') == '2':                 # 浮點數倍數 5~11 項後退
            f_list = [5, 8, 13, 21, 34, 55, 89]
        elif os.environ.get('mode') == '3':                 # 整數倍數 3~11 項後退
            rand_int = True
            f_list = [2, 3, 5, 8, 13, 21, 34, 55, 89]
        elif os.environ.get('mode') == '4':                 # 浮點數倍數 3~11 項後退
            f_list = [2, 3, 5, 8, 13, 21, 34, 55, 89]
        elif os.environ.get('mode') == '5':                 # 整數倍數 4~11 項後退
            rand_int = True
            f_list = [3, 5, 8, 13, 21, 34, 55, 89]
        elif os.environ.get('mode') == '6':                 # 浮點數倍數 4~11 項後退
            f_list = [3, 5, 8, 13, 21, 34, 55, 89]
        elif os.environ.get('mode') == '7':                 # 整數倍數 5~12 項後退
            rand_int = True
            f_list = [5, 8, 13, 21, 34, 55, 89, 144]
        elif os.environ.get('mode') == '8':                 # 浮點數倍數 5~12 項後退
            f_list = [5, 8, 13, 21, 34, 55, 89, 144]
        elif os.environ.get('mode') == '9':                 # 整數倍數 6~11 項後退
            rand_int = True
            f_list = [8, 13, 21, 34, 55, 89]
        elif os.environ.get('mode') == '10':                # 浮點數倍數 6~11 項後退
            f_list = [8, 13, 21, 34, 55, 89]
        elif os.environ.get('mode') == '11':                # 整數倍數 6~12 項後退
            rand_int = True
            f_list = [8, 13, 21, 34, 55, 89, 144]
        elif os.environ.get('mode') == '12':                # 浮點數倍數 6~12 項後退
            f_list = [8, 13, 21, 34, 55, 89, 144]
        elif os.environ.get('mode') == '101':               # 1/2 * 浮點數倍數 5~11 項後退
            f_list = [n / 2 + 1 for n in [5, 8, 13, 21, 34, 55, 89]]
        elif os.environ.get('mode') == '102':               # 1/3 * 浮點數倍數 5~11 項後退
            f_list = [n / 3 + 1 for n in [5, 8, 13, 21, 34, 55, 89]]
        elif os.environ.get('mode') == '103':               # 1/5 * 浮點數倍數 5~11 項後退
            f_list = [n / 5 + 1 for n in [5, 8, 13, 21, 34, 55, 89]]
        elif os.environ.get('mode') == '104':               # 1/10 * 浮點數倍數 5~11 項後退
            f_list = [n / 10 + 1 for n in [5, 8, 13, 21, 34, 55, 89]]
        elif os.environ.get('mode') == '105':               # 1/100 * 浮點數倍數 5~11 項後退
            f_list = [n / 100 + 1 for n in [5, 8, 13, 21, 34, 55, 89]]
        else:                                               # 預設： 5 ~ 11 項浮點數後退
            f_list = [5, 8, 13, 21, 34, 55, 89]
        
        i = 0

        while True:
            if delay_until > self.network.t_now:
                yield 'delay'
                continue

            if startFlag:
                startFlag = False
                sendPacket.source.startCAD()
                sendPacket.cad_counter += 1
                # 存活時間(能量)處理
                if sendPacket.source.options.get('battery_life') and \
                   sendPacket.source.battery_life >= 10:
                    sendPacket.source.battery_life -= 10
                yield 'not done'

            status = sendPacket.source.checkCAD()
            # CAD 偵測失敗，頻道上有資料正在傳輸
            if status == -1:
                failure_counter += 1
                startFalg = True
                CAD_success_counter = 0

                # 後退
                if rand_int:
                    d_cad = np.random.randint(low=0, high=f_list[i]+1)
                else:
                    d_cad = np.random.random() * f_list[i]

                # 後退計數
                if self.backoff_counter.get(f_list[i]):
                    self.backoff_counter[f_list[i]] += 1
                else:
                    self.backoff_counter[f_list[i]] = 1

                d_cad *= self.max_toa
                delay_until = self.network.t_now + d_cad

                while True:
                    if delay_until > self.network.t_now:
                        yield 'backoff delay'
                    else:
                        if i + 1 < len(f_list):
                            i += 1
                        break

            elif status == 0:
                yield 'not done'
            # 如果頻道上沒有人正在傳輸
            else:
                # 後退成功次數如果 >= 設定次數，預設是2
                if CAD_success_counter >= int(os.environ.get('CAD_T', 2)):
                    if sendPacket.source.options.get('battery_life') and \
                       sendPacket.source.battery_life >= 55:
                        sendPacket.source.battery_life -= 55
                    yield 'success'

                # delay 後再次偵測
                if rand_int:
                    d_cad = np.random.randint(low=0, high=f_list[i]+1)
                else:
                    d_cad = np.random.random() * f_list[i]

                # 後退計數
                if self.backoff_counter.get(f_list[i]):
                    self.backoff_counter[f_list[i]] += 1
                else:
                    self.backoff_counter[f_list[i]] = 1
                    
                d_cad *= self.max_toa
                delay_until = self.network.t_now + d_cad

                while True:
                    if delay_until > self.network.t_now:
                        yield 'delay'
                    else:
                        break

                startFlag = True
                CAD_success_counter += 1
                
                

    def gateway_run(self, gateway):
        # gateway 接收 判斷碰撞
        gateway.checkReciveAllData()

        yield 'pass'
        return
    