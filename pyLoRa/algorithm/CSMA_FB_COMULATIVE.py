from pyLoRa.packet import Packet
import numpy as np
import os

class CSMA_FB_COMULATIVE:
    def __init__(self, network):
        self.status = ()
        self.network = network
        self.max_failure = network.max_failure
        self.cad_base_counter = 9
        self.max_toa = Packet(None, 'a' * 255).setFromNetwork(network).toa # 最大封包 255
        self.backoff_counter = {}
        
        print(f'max_toa: {self.max_toa}')

    def sensor_run(self, sendPacket):
        failure_counter = 0
        delay_until = 0

        startFlag = True
        CAD_success_counter = 0
        CAD_success_comulative_counter = 0      # 增加累積確認使用的 counter
        rand_int = False
        # 使用費氏數列 5~11 項去做運算
        f_list = [5, 8, 13, 21, 34, 55, 89]
        
        i = 0

        while True:
            if delay_until > self.network.t_now:
                yield 'delay'
                continue

            if startFlag:
                startFlag = False
                sendPacket.source.startCAD()
                sendPacket.cad_counter += 1
                yield 'not done'

            status = sendPacket.source.checkCAD()
            # CAD 偵測失敗，頻道上有資料正在傳輸
            if status == -1:
                failure_counter += 1
                startFalg = True
                # first_CAD_success = False 
                CAD_success_counter = 0

                # 後退
                if rand_int:
                    d_cad = np.random.randint(low=0, high=f_list[i]+1)
                else:
                    d_cad = np.random.random() * f_list[i]

                # 後退計數
                if self.backoff_counter.get(f_list[i]):
                    self.backoff_counter[f_list[i]] += 1
                else:
                    self.backoff_counter[f_list[i]] = 1

                d_cad *= self.max_toa
                delay_until = self.network.t_now + d_cad

                while True:
                    if delay_until > self.network.t_now:
                        yield 'backoff delay'
                    else:
                        if i + 1 < len(f_list):
                            i += 1
                        break

            elif status == 0:
                yield 'not done'
            # 如果頻道上沒有人正在傳輸
            else:
                # 後退成功次數如果 >= 設定次數，預設是2
                if CAD_success_counter >= int(os.environ.get('CAD_T', 2)):
                    yield 'success'

                # 如果超出數列的一半改用累積確認，成功次數預設是2
                if i >= len(f_list) / 2 and CAD_success_comulative_counter >= int(os.environ.get('CAD_CT', 2)):
                    yield 'success'


                # delay 後再次偵測
                if rand_int:
                    d_cad = np.random.randint(low=0, high=f_list[i]+1)
                else:
                    d_cad = np.random.random() * f_list[i]

                # 後退計數
                if self.backoff_counter.get(f_list[i]):
                    self.backoff_counter[f_list[i]] += 1
                else:
                    self.backoff_counter[f_list[i]] = 1
                    
                d_cad *= self.max_toa
                delay_until = self.network.t_now + d_cad

                while True:
                    if delay_until > self.network.t_now:
                        yield 'backoff delay'
                    else:
                        break

                startFlag = True
                CAD_success_counter += 1
                CAD_success_comulative_counter += 1
                
                

    def gateway_run(self, gateway):
        # gateway 接收 判斷碰撞
        gateway.checkReciveAllData()

        yield 'pass'
        return
    