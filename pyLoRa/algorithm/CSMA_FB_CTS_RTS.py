from pyLoRa.packet import Packet
import numpy as np

class CSMA_FB_CTS_RTS:
    def __init__(self, network):
        self.status = ()
        self.network = network
        self.max_failure = network.max_failure
        self.cad_base_counter = 9
        self.max_toa = Packet(None, 'a' * 255).setFromNetwork(network).toa # 最大封包 255
        self.t_difs = self.max_toa * 1
        self.t_sifs = self.max_toa * 0.3
        self.reciveRTS = None
        self.reciveData = None

        
        print(f'max_toa: {self.max_toa}')

    def sensor_run(self, sendPacket):
        failure_counter = 0
        delay_until = 0

        startFlag = True
        first_CAD_success = False
        # 去掉 [1, 2, 3] 原因：範圍太小，碰撞機率太大
        f_list = [5, 8, 13, 21, 34, 55, 89]
        i = 0
        hit_flag = False

        while True:
            if delay_until > self.network.t_now:
                yield 'delay'
                continue

            if startFlag:
                startFlag = False
                sendPacket.source.startCAD()
                sendPacket.cad_counter += 1
                yield 'not done'

            status = sendPacket.source.checkCAD()
            if status == -1:
                hit_flag = True

            elif status == 0:
                yield 'not done'
            else:
                # 沒發現碰撞
                # 等待 difs 時間
                delay_until = self.network.t_now + self.t_difs

                while True:
                    if delay_until > self.network.t_now:
                        yield 'difs delay'
                    else:
                        yield 'rts'
                        break

                # 傳送 RTS 之後最多等待 2ToA max
                listen_until = self.network.t_now + self.max_toa * 2

                while True:
                    if listen_until >= self.network.t_now:
                        packet = sendPacket.source.checkReciveCTXData()

                        if packet and packet.allow_id == sendPack.source.id:
                            yield 'ctx'
                            break
                        else:
                            yield 'waiting ctx'
                    else:
                        hit_flag = True
                        yield 'timeout'
                        break

                # 如果接收到 CTX，等待 SIFS 後傳送資料
                delay_until = self.network.t_now + self.t_sifs

                while True and not hit_flag:
                    if delay_until > self.network.t_now:
                        yield 'sifs delay'
                    else:
                        yield 'data'
                        break

                # 傳送資料後最多等待 2ToA max

                listen_until = self.network.t_now + self.max_toa * 2

                while True and not hit_flag:
                    if listen_until >= self.network.t_now:
                        packet = sendPacket.source.checkReciveACKData()

                        if packet and packet.allow_id == sendPack.source.id:
                            hit_flag = False
                            yield 'ack'
                            break
                        else:
                            yield 'waiting ack'
                    else:
                        hit_flag = True
                        yield 'timeout'
                        break
            
            if hit_flag:
                d_cad = np.random.randint(low=0, high=f_list[i]+1)
                d_cad *= self.max_toa
                delay_until = self.network.t_now + d_cad
                failure_counter += 1

                while True:
                    if delay_until > self.network.t_now:
                        yield 'backoff delay'
                    else:
                        break

                startFlag = True

            if failure_counter >= 20:
                yield 'failure'
                
                

    def gateway_run(self, gateway):
        # gateway 接收 判斷碰撞
        reciveData = gateway.checkReciveAllData()

        if not self.reciveRTS:
            self.reciveRTS = gateway.checkReciveRTXData()

        if not self.reciveData:
            self.reciveData = gateway.checkReciveAllData()

        if self.reciveRTS:

            delay_until = self.network.t_now + self.t_sifs

            while True:
                if delay_until > self.network.t_now:
                    yield 'sifs delay'
                else:
                    print('???')
                    packet = Packet(self, 'reRTS')
                    packet.type = 2
                    packet.allow_id = self.reciveRTS.source.id
                    self.reciveRTS = None
                    packet.setFromNetwork(self.network)
                    self.network.packets.append(packet)
                    yield 'rts'
                    break

        elif self.reciveData:
            delay_until = self.network.t_now + self.t_sifs

            while True:
                if delay_until > self.network.t_now:
                    yield 'sifs delay'
                else:
                    packet = Packet(self, 'revDa')
                    packet.type = 3
                    packet.allow_id = self.reciveData.source.id
                    self.reciveData = None
                    packet.setFromNetwork(self.network)
                    self.network.packets.append(packet)
                    yield 'ack'
                    break
        else:
            yield 'pass'
    