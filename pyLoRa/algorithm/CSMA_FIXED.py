from pyLoRa.packet import Packet
import numpy as np
import os

class CSMA_FIXED:
    def __init__(self, network):
        self.status = ()
        self.network = network
        self.max_failure = network.max_failure
        self.cad_base_counter = 9
        self.max_toa = Packet(None, 'a' * 255).setFromNetwork(network).toa # 最大封包 255

        print(f'max_toa: {self.max_toa}')

    def sensor_run(self, sendPacket):
        failure_counter = 0
        delay_until = 0

        startFlag = True
        fl = False

        while True:
            if delay_until > self.network.t_now:
                yield 'delay'
                continue

            if startFlag:
                startFlag = False
                sendPacket.source.startCAD()
                sendPacket.cad_counter += 1
                yield 'not done'

            status = sendPacket.source.checkCAD()
            #　如果發現有正在傳輸
            if status == -1:
                failure_counter += 1
                startFalg = True

                # 從環境變數讀取 RANDOM_MAX ，作為後退的 max　值
                d_cad = self.max_toa + np.random.random() * float(os.environ.get('RANDOM_MAX', self.max_toa))
                delay_until = self.network.t_now + d_cad

                while True:
                    if delay_until > self.network.t_now:
                        yield 'backoff delay'
                    else:
                        fl = False
                        break

            elif status == 0:
                yield 'not done'
            else:
                # 沒發現頻道上有資料正在傳輸，delay 時間使用 ToAmax
                startFlag = True
                if fl:
                    yield 'success'
                else:
                    t_rb = self.max_toa
                    delay_until = self.network.t_now + t_rb

                    while True:
                        if delay_until > self.network.t_now:
                            yield 'delay'
                        else:
                            fl = True
                            break            

    def gateway_run(self, gateway):
        # gateway 接收 判斷碰撞
        gateway.checkReciveAllData()

        yield 'pass'
        return
    