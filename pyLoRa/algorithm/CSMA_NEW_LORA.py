from pyLoRa.packet import Packet
import numpy as np

class CSMA_NEW_LORA:
    def __init__(self, network):
        self.status = ()
        self.network = network
        self.max_failure = network.max_failure
        self.cad_base_counter = 9
        self.max_toa = Packet(None, 'a' * 255).setFromNetwork(network).toa # 最大封包 255
        
        print(f'max_toa: {self.max_toa}')

    def sensor_run(self, sendPacket):
        failure_counter = 0
        delay_until = 0

        step_time = (self.max_toa - self.network.t_cad) / (self.cad_base_counter - 1) - self.network.t_cad

        # 防止有兩個 CAD 同時進行
        if step_time < 0:
            step_time = 0

        step_counter = 0
        startFlag = True
        finalCad = False
        cad_counter = np.random.randint(low=1, high=self.cad_base_counter*2)
        backupFlag = True
        delayTimes = 1

        while True:
            if delay_until > self.network.t_now:
                yield 'delay'
                continue
            # 開始偵測
            if startFlag:
                startFlag = False
                sendPacket.cad_counter += 1
                sendPacket.source.startCAD()
                yield 'not done'

            status = sendPacket.source.checkCAD()

            # CAD 偵測失敗，頻道上有資料正在傳輸
            if status == -1:
                failure_counter += 1
                # step_counter = 0
                startFlag = True
                delay_until = self.network.t_now + \
                    np.random.randint(low=1, high=self.cad_base_counter * delayTimes) * (self.network.t_cad + step_time)
                if delayTimes < 3:
                    delayTimes += 1

                if backupFlag:
                    backupFlag = False
                    delay_until += self.max_toa

                yield 'not done'

            elif status == 0:
                yield 'not done'
            else:
                # 沒發現碰撞
                step_counter += 1

                # 偵測成功次數 等於 需要偵測的次數...
                if step_counter == cad_counter:
                    yield 'success'
                
                # end

                yield 'not done'
                

    def gateway_run(self, gateway):
        # gateway 接收 判斷碰撞
        gateway.checkReciveAllData()

        yield 'pass'
        return
    