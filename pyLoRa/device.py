import math
import os
import json

from pyLoRa.packet import Packet

class Device:
    def __init__(self):
        self.cad_start_time = None
        self.x = 0
        self.y = 0
        self.options = {}

    # 把 CAD 的啟動時間改成現在時間就是啟動偵測傳輸
    def startCAD(self):
        self.cad_start_time = self.network.t_now
        
    def checkCAD(self):
        self.cad_hit = False
        # 偵測現在在空中傳輸的資料，如果發現有資料正在傳輸，且跟 CAD時間有重疊的話，則把cad_hit設為True
        for packet in [p for p in self.network.packets if p.send_time < self.network.t_now - self.network.time_step / 2]: # tick / 2 是為了避免浮點數誤差
            if packet.send_time < self.cad_start_time and \
               self.cad_start_time + self.network.t_cad < packet.send_time + packet.toa:

                self.cad_hit = True

        if self.cad_start_time + self.network.t_cad < self.network.t_now:
            if self.cad_hit:
                return -1 # 碰撞
            return 1      # 能傳送出去
        return 0          # 還沒偵測結束


    def checkReciveAllData(self):
        hit_flag = False
        first_packet = None
        for packet in self.network.packets:
            # 如果現在的 tick 有封包正在傳輸
            if packet.send_time + packet.toa > self.network.t_now:
                if not first_packet:
                    first_packet = packet # 第一個封包，即前面沒有偵測到封包
                else:   # 如果有第二個或以後的封包正在傳輸，則把他們標示成會碰撞
                    first_packet.flag_for_gateway = True
                    packet.flag_for_gateway = True
                    hit_flag = True

                    # 如果是 Append ，則把內的所有資料都標示成會碰撞
                    if self.options and self.options.get('append'):
                        data = packet.data
                        data = json.loads(data)
                        for d in packet.source.data_history:
                            for d2 in data:
                                if d['_t'] == d2['_t']:
                                    d['status'] = 'hit'

                        data = first_packet.data
                        data = json.loads(data)
                        for d in first_packet.source.data_history:
                            for d2 in data:
                                if d['_t'] == d2['_t']:
                                    d['status'] = 'hit'

                    # 如果是 Aggregate ，則把該 sensor 時間範圍內的資料都標示成會碰撞
                    if self.options and self.options.get('aggregate'):
                        paddingTime = packet.paddingTime
                        lastUpdateTime = packet.lastUpdateTime

                        for d in packet.source.data_history:
                            if paddingTime <= d['_t'] and d['_t'] <= lastUpdateTime:
                                d['status'] = 'hit'
                        
                        paddingTime = first_packet.paddingTime
                        lastUpdateTime = first_packet.lastUpdateTime

                        for d in first_packet.source.data_history:
                            if paddingTime <= d['_t'] and d['_t'] <= lastUpdateTime:
                                d['status'] = 'hit'
                            

        if hit_flag:
            return False

        return first_packet

    def checkReciveRTXData(self):
        rtx_packets = [p for p in self.network.packets if p.type == 1]
        
        for packet in rtx_packets:
            if packet.send_time + packet.toa < self.network.t_now and not packet.flag_for_gateway:
                return packet

        return False

    def checkReciveCTXData(self):
        ctx_packets = [p for p in self.network.packets if p.type == 2]
        
        for packet in ctx_packets:
            if packet.send_time + packet.toa < self.network.t_now and not packet.flag_for_gateway:
                return packet

        return False

    def checkReciveCTXData(self):
        ack_packets = [p for p in self.network.packets if p.type == 3]
        
        for packet in ack_packets:
            if packet.send_time + packet.toa < self.network.t_now and not packet.flag_for_gateway:
                return packet

        return False


class Sensor(Device):
    def __init__(self, buffer_size=35, options={}):
        self.id = 0
        self.padding_data = []      # buffer 
        self.data_history = []      # 產生過的歷史資料
        self.aggregate_packet = []  # aggregate 後的封包
        self.network = None         # 網路環境，用已讀取網路參數
        self.t_cad = 0              # 偵測碰撞時間
        self.time_to_send_next_packet = 0   # 下一個可傳送封包的時間，用於 aloha + duty cycle ，及一開始的設定的開始發送時間
        self.battery_life = options.get('battery_life') # 如果有設定 存活時間則讀取存活時間，沒有則為 None

        self.cad_start_time = 0
        self.cad_hit = False        
        self.status = ''

        self.failure_counter = 0    # 傳送失敗的 counter
        self.success_counter = 0    # 傳送失敗的 counter
        self.drop_counter = 0       # 封包被丟棄的 counter  
        self.want_send_data_counter = 0 # 資料建立，想要建立封包的 counter
        self.agg_counter = 0        # aggregate 次數的 counter
        self.append_counter = 0     # append 次數的 counter
        self.padding = None         # padding packet，存放目前要被發送的封包，是一個 iterator，成功會 yield success

        self.out_of_date_packet_counter = 0 # 沒使用到，紀錄過時封包
        self.buffer_size = buffer_size  # buffer seize
        self.options = options      # 其他的參數

    # 計算與 gateway 的距離，沒用到
    def distanceToGateway(self, gateway):
        return math.sqrt((self.x - gateway.x)**2 + (self.y - gateway.y) ** 2)

    # 設定網路，用以讀取網路參數
    def setNetwork(self, network):
        self.network = network

    # 把資料增加到 buffer 中
    def addDataToPadding(self, data={}):
        self.want_send_data_counter += 1
        # 如果是有意義的資料，則在 sensor 的歷史資料內增加此筆紀錄
        if self.options.get('packet_length') == -1:
            self.data_history.append({"_t": self.network.t_now, "temp": data.get('temp')})

        # 如果 buffer 已滿，就丟棄
        if len(self.padding_data) >= self.buffer_size:
            self.drop_counter += 1
            return
        
        # 如果要合併封包
        if self.options.get('append'):
            if len(self.padding_data) > 0:
                d = self.padding_data[-1].data
                d = json.loads(d)
                d.append({"_t": self.network.t_now, "temp": data.get('temp')})

                d_d = json.dumps(d)
                
                # 如果合併起來封包會過大就新建封包
                if len(d_d) > 255:
                    packet = Packet(self, json.dumps([{"_t": self.network.t_now, "temp": data.get('temp')}]))
                    packet.setFromNetwork(self.network)

                    self.padding_data.append(packet)
                else:
                    self.padding_data[-1].setData(d_d)
                    self.append_counter += 1
            else:
                packet = Packet(self, json.dumps([{"_t": self.network.t_now, "temp": data.get('temp')}]))
                packet.setFromNetwork(self.network)

                self.padding_data.append(packet)

        # 如果要封包彙整
        elif self.options.get('aggregate'):
            if len(self.padding_data) == 0:
                new_data = {}

                for key, value in data.items():
                    if key.startswith('_'):
                        pass
                    
                    new_data[f'_{key}_w'] = 1         # 權重數量
                    new_data[f'_{key}_x'] = data[key] # max
                    new_data[f'_{key}_n'] = data[key] # min
                    new_data[f'{key}'] = data[key]    # 平均

                packet = Packet(self, json.dumps(new_data))
                packet.setFromNetwork(self.network)

                self.padding_data.append(packet)
            else:
                new_packet_flag = False

                packet_data = self.padding_data[-1].data
                packet_data = json.loads(packet_data)

                for key, value in packet_data.items():
                    if key.startswith('_'):
                        pass
                    
                    if packet_data.get(f'_{key}_w') and data.get(key):
                        # 如果大於誤差值，則建立一個新的封包
                        if abs((data[key] - packet_data[f'{key}']) / packet_data[f'{key}']) > 0.5:
                            new_packet_flag = True
                            break


                        # 平均值
                        packet_data[f'{key}'] = (packet_data[f'_{key}_w'] * packet_data[key] + data[key] )/ (packet_data[f'_{key}_w'] + 1)
                        packet_data[f'_{key}_w'] += 1
                        if packet_data[f'_{key}_x'] < data[key]:
                            packet_data[f'_{key}_x'] = data[key] # max

                        if packet_data[f'_{key}_n'] > data[key]:
                            packet_data[f'_{key}_n'] = data[key] # min
                
                if new_packet_flag:
                    new_data = {}
                    for key, value in data.items():
                        if key.startswith('_'):
                            pass
                        
                        new_data[f'_{key}_w'] = 1
                        new_data[f'_{key}_x'] = data[key] # max
                        new_data[f'_{key}_n'] = data[key] # min
                        new_data[f'{key}'] = data[key]

                    packet = Packet(self, json.dumps(new_data))
                    packet.setFromNetwork(self.network)

                    self.padding_data.append(packet)
                else:
                    self.padding_data[-1].setData(json.dumps(packet_data))
                    self.agg_counter += 1
        else:
            packet = Packet(self, json.dumps(data))
            packet.setFromNetwork(self.network)

            self.padding_data.append(packet)

    # 丟棄過期的封包，沒用到
    def checkOutOfDatePacket(self):
        if self.options.get('checkOutOfDatePacketTime'):
            print('coodpt')
            tmp = [p for p in self.padding_data \
                if self.network.t_now > \
                    p.paddingTime + self.options['checkOutOfDatePacketTime']]
            self.out_of_date_packet_counter += len(self.padding_data) - len(tmp)
            self.padding_data = tmp

    def run(self):
        # 如果 buffer 內沒有封包就不用跑拉
        if len(self.padding_data) > 0:
            # 確定封包有沒有過期
            self.checkOutOfDatePacket()
            if self.time_to_send_next_packet > self.network.t_now:
                return

            # 如果前面的封包已發送完畢，發送下一個封包
            if not self.padding:
                self.padding = self.network.ca_algorithm.sensor_run(self.padding_data[0])
            
            status = next(self.padding)
            # print(f"id: {self.id}, tts: {self.time_to_send_next_packet}, status: {status}")

            # print(self.padding_data[0].id, status)

            if status == 'failure':
                self.padding_data.pop(0)
                self.failure_counter += 1
                self.padding = None

            # 如果偵測到頻道是空的
            elif status == 'success':
                # 發送下一個封包的時間至少是這封包發送完的時間，或是 duty cycle 的時間 (algorithm aloha 會改變 time_to_send_next_packet 的值)
                self.time_to_send_next_packet = max(self.network.t_now + self.padding_data[0].toa, self.time_to_send_next_packet)
                # 把封包丟到網路上，並且把發送時間改成現在
                self.padding_data[0].send_time = self.network.t_now
                self.network.packets.append(self.padding_data[0])
                # 如果是使用 aggregate 則紀錄這個封包
                if self.options and self.options.get('aggregate'):
                    self.aggregate_packet.append(self.padding_data[0])

                # 然後把封包從 buffer 內移除
                self.padding_data.pop(0)
                self.success_counter += 1
                self.padding = None

            elif status == 'rts':
                packet = Packet(self, 6) # rts flag + id(4 byte) + size(1 byte)
                packet.type = 1
                packet.setFromNetwork(self.network)
                packet.send_time = self.network.t_now
                self.time_to_send_next_packet = max(self.network.t_now + packet.toa, self.time_to_send_next_packet)
                self.network.packets.append(packet)

            elif status == 'data':
                self.padding_data[0].send_time = self.network.t_now
                self.time_to_send_next_packet = max(self.network.t_now + packet.toa, self.time_to_send_next_packet)
                self.network.packets.append(self.padding_data[0])


            elif status == 'ack':
                self.padding_data.pop(0)
                self.success_counter += 1
                self.padding = None

                


class Gateway(Device):
    def __init__(self, options={}):
        self.id = 0
        self.network = None
        self.options = options

    def setNetwork(self, network):
        self.network = network
    
    def run(self):
        status = next(self.network.ca_algorithm.gateway_run(self))
    