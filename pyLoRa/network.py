import os
import json

from pyLoRa import device, packet

# 匯入各種碰撞避免演算法
from pyLoRa.algorithm.ALOHA import ALOHA
from pyLoRa.algorithm.CSMA_NEW_LORA import CSMA_NEW_LORA
from pyLoRa.algorithm.CSMA_BEH import CSMA_BEH
from pyLoRa.algorithm.CSMA_FB import CSMA_FB
from pyLoRa.algorithm.CSMA_FB2 import CSMA_FB2
from pyLoRa.algorithm.CSMA_FB_CTS_RTS import CSMA_FB_CTS_RTS
from pyLoRa.algorithm.CSMA_FB_COMULATIVE import CSMA_FB_COMULATIVE
from pyLoRa.algorithm.CSMA_FIXED import CSMA_FIXED

import numpy as np

class NetworkSetting:
    # LoRaWAN 的各預設參數
    def __init__(self, np=1, sf=11, bw=125000, code_rate=1, crc=True, nonheader=True, de=True):
        self.np = np
        self.sf = sf
        self.bw = bw
        self.crc = crc
        self.nonheader = nonheader
        self.de = de
        self.code_rate = code_rate
        self.Ts = 2 ** sf / bw

class Network:
    # static ，模擬共用參數， 純粹判斷是使用哪種演算法
    MODE_ALOHA = 0
    MODE_CSMA_NEW_LORA = 1
    MODE_CSMA_BEH = 2
    MODE_CSMA_FB = 3
    MODE_CSMA_FB_CTS_RTS = 4
    MODE_CSMA_FIXED = 5
    MODE_CSMA_FB_COMULATIVE = 6
    MODE_CSMA_FB2 = 7

    def __init__(self, end_time=60, time_step=0.01,\
        networkSetting=NetworkSetting(), mode=MODE_ALOHA, \
        period=100, dumpFileName='sim.data', options = {}):

        # 重設封包 id        
        packet.resetPacketId()

        self.gateways = []  # 場域內的 gateway
        self.sensors = []   # 場域內的 sensors
        self.packets = []   # 在實驗內正在被傳送的封包，傳送完畢會被移除，等待傳送的封包會在 sensor 內的buffer中
        self.x_size = 0     # 場域大小 x
        self.y_size = 0     # 場域打小 y
        self.mode = mode    # 使用哪種碰撞演算法
        self.period = period / time_step    # 發送頻率

        self.setting = networkSetting       # 網路設定
        
        self.time_step = time_step          # tick 長度
        self.t_now = 0                      # 模擬的現在時間
        self.end_time = end_time            # 模擬的結束時間
        self.max_failure = 2147483647       # 封包的發送失敗次數，大於此值會被丟棄
        
        self.t_cad = 2 * self.setting.Ts    # 偵測的 CAD 時間
        print(f"t_cad: {self.t_cad}")
        
        # 設定發送的演算法，每個演算法內都必須有 sensor_run 與 gateway_run
        if self.mode == self.MODE_CSMA_NEW_LORA:
            self.ca_algorithm = CSMA_NEW_LORA(self)
        elif self.mode == self.MODE_CSMA_BEH:
            self.ca_algorithm = CSMA_BEH(self)
        elif self.mode == self.MODE_CSMA_FB:
            self.ca_algorithm = CSMA_FB(self)
        elif self.mode == self.MODE_CSMA_FB2:
            self.ca_algorithm = CSMA_FB2(self)
        elif self.mode == self.MODE_CSMA_FB_CTS_RTS:
            self.ca_algorithm = CSMA_FB_CTS_RTS(self)
        elif self.mode == self.MODE_CSMA_FIXED:
            self.ca_algorithm = CSMA_FIXED(self)
        elif self.mode == self.MODE_CSMA_FB_COMULATIVE:
            self.ca_algorithm = CSMA_FB_COMULATIVE(self)
        else:
            self.ca_algorithm = ALOHA(self)

        # 其餘設定。
        self.options = options
        # 輸出檔名
        self.dumpFileName = dumpFileName
        self.dump = open(dumpFileName, "w")
        # 輸出資訊，除錯用
        self.showInfo()

    def showInfo(self):
        if self.mode == self.MODE_CSMA_NEW_LORA:
            print('mode: CSMA_NEW_LORA')
        elif self.mode == self.MODE_CSMA_BEH:
            print('mode: CSMA_BEH')
        elif self.mode == self.MODE_CSMA_FB:
            print('mode: CSMA_FB')
        elif self.mode == self.MODE_CSMA_FB2:
            print('mode: CSMA_FB2')
        elif self.mode == self.MODE_CSMA_FB_CTS_RTS:
            print('mode: CSMA_FB_CTS_RTS')
        elif self.mode == self.MODE_CSMA_FIXED:
            print('mode: CSMA_FIXED')
        elif self.mode == self.MODE_CSMA_FB_COMULATIVE:
            print('mode: CSMA_FB_COMULATIVE')
        else:
            print('mode: ALOHA')
        

    def run(self):

        packet_counter = 0
        packet_hit_counter = 0
        packet_cad_counter = 0

        packet_length_type = self.options.get('packet_length', -1)

        while True:
            # 如果模擬結束了
            if self.end_time < self.t_now:
                # 輸出封包統計
                self.dump.write(f"c_packet, {packet_counter}, {packet_cad_counter}, {packet_hit_counter}, {(packet_counter - packet_hit_counter) / packet_counter * 100 :.3f}%")

                # 輸出發送節點資訊
                self.dump.write("\n\n")
                for s in self.sensors:
                    self.dump.write(f"sensor, {s.id}, {s.want_send_data_counter}, {len(s.padding_data)}, {s.success_counter}, {s.drop_counter}, {s.failure_counter}, {s.agg_counter}, {s.append_counter}, {s.out_of_date_packet_counter}")
                    if self.options.get('battery_life'):
                        if s.battery_life <= 9:
                            self.dump.write(f", dead")
                        else:
                            self.dump.write(f", live")
                
                    self.dump.write(f"")
                    self.dump.write('\n')
                self.dump.close()

                # 輸出每個節點的資料資訊
                if self.options.get('packet_length') == -1:                
                    sd = {}

                    for s in self.sensors:
                        sd[f"s{s.id}"] = s.data_history

                    with open(f"{self.dumpFileName}.sensor", "w") as f:
                        f.write(json.dumps(sd))

                # 輸出Aggregate 的成功發送封包
                if self.options and self.options.get('aggregate'):
                    sd2 = {}
                    for s in self.sensors:
                        d2 = []
                        for ap in s.aggregate_packet:
                            d2.append({
                                'id': ap.id,
                                'paddingTime': ap.paddingTime,
                                'lastUpdateTime': ap.lastUpdateTime,
                                'data': json.loads(ap.data)
                            })
                        sd2[f"s{s.id}"] = d2

                    with open(f"{self.dumpFileName}.aggregate.success.sensor", "w") as f:
                        f.write(json.dumps(sd2))

                # 如果使用 費氏數列後退則輸出後退的次數統計
                if self.mode == self.MODE_CSMA_FB:
                    with open(f"{self.dumpFileName}.FB-backoff-counter.json", 'w') as f:
                        f.write(json.dumps(self.ca_algorithm.backoff_counter))

                return 

            for sensor in self.sensors:
                # 如果跑生存時間(電量)且沒電了就被忽略
                if self.options.get('battery_life') and sensor.battery_life <= 9:
                    continue
                
                if np.random.uniform(0, 1) <= 1 / self.period:
                    r = np.random.randint(low=0, high=100)

                    # 如果封包型態是 -1 ，則使用時間的模擬資料
                    if packet_length_type == -1:
                        if r <= 5:
                            t = 100
                        else:
                            t = np.random.randint(low=20, high=30)
                        # 增加封包到發送的 queue
                        sensor.addDataToPadding({'temp': t})

                    # type 0 則使用 5 bytes ~ 255 bytes 的資料
                    elif packet_length_type == 0:
                        sensor.addDataToPadding('A' * np.random.randint(low=5, high=255+1))
                    # type 1 則使用 5 bytes ~ 100 bytes 的資料
                    elif packet_length_type == 1:
                        sensor.addDataToPadding('A' * np.random.randint(low=5, high=100+1))
                    # type 2 則使用 101 bytes ~ 200 bytes 的資料
                    elif packet_length_type == 2:
                        sensor.addDataToPadding('A' * np.random.randint(low=101, high=200+1))
                    # type 3 則使用 201 bytes ~ 255 bytes 的資料
                    elif packet_length_type == 3:
                        sensor.addDataToPadding('A' * np.random.randint(low=201, high=255+1))
                # 節點進行下一步的動作
                sensor.run()
                
            for gateway in self.gateways:
                # gateway 執行下一步的動作
                gateway.run()
            
            # 封包完全發送完畢，會增加 counter，並紀錄到檔案
            for p in [p for p in self.packets if p.send_time + p.toa < self.t_now]:
                packet_counter += 1
                packet_cad_counter += p.cad_counter

                if p.flag_for_gateway:
                    packet_hit_counter += 1
                
                self.dump.write(f"packet, {p.id}, {p.source.id}, {p.type}, {p.send_time}, {p.cad_counter}, {p.toa_tpr}, {p.flag_for_gateway}\n")

            # 去除已完全發送完畢的封包
            self.packets = [p for p in self.packets if p.send_time + p.toa >= self.t_now]
            
            # 把時間++，然後取到小數點以下第三位取整
            self.t_now += self.time_step
            self.t_now = float(f"{self.t_now:.3f}")
            yield self.t_now


        
    # 設定場域大小
    def setRange(self, x_size, y_size):
        self.x_size = x_size
        self.y_size = y_size

    # 增加 gateway
    def addGateway(self, x_pos, y_pos, options={}):
        gateway = device.Gateway(options=options)

        gateway.x = x_pos
        gateway.y = y_pos
        gateway.id = len(self.gateways)
        gateway.setNetwork(self)

        self.gateways.append(gateway)

        return gateway

    # 增加感測器
    def addSensor(self, x_pos, y_pos, options={}):
        if options.get('buffer_size'):
            sensor = device.Sensor(buffer_size=options['buffer_size'], options=options)
        else:
            sensor = device.Sensor(options=options)
            

        sensor.x = x_pos
        sensor.y = y_pos
        sensor.id = len(self.sensors)
        sensor.setNetwork(self)

        self.sensors.append(sensor)

        return sensor