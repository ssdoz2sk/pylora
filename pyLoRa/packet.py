import math

packet_id = 0

def resetPacketId():
    global packet_id
    packet_id = 0

class Packet:
    def __init__(self, source, data):
        self.source = source
        self.network = None
        self.cad_counter = 0
        self.paddingTime = -1
        self.lastUpdateTime = -1

        self.toa = 0 # 晶片往空氣中傳輸，而空氣中傳輸速度 = 光速
        self.toa_tpr = 0 # Preambles Code 所需要傳送的時間
        self.send_time = 0
        self.flag_for_gateway = False

        self.type = 0 # 0 data, 1 rts, 2 cts, 3 ack
        self.allow_id = -1
        self.data = data
        self.pl = len(self.data)
        
        global packet_id
        self.id = packet_id
        packet_id += 1
        
    def setData(self, data):
        self.data = data
        self.pl = len(self.data)
        self.lastUpdateTime = self.network.t_now

    def setFromNetwork(self, network):
        self.network = network
        self.toa_tpr = self.calTpr()
        self.toa = self.calTpl() + self.toa_tpr
        self.paddingTime = network.t_now
        self.lastUpdateTime = network.t_now
        
        return self

    def calTpr(self):
        return (self.network.setting.np + 4.25) * self.network.setting.Ts

    def calTpl(self):
        return (8 + \
            max(self.fi() *(self.network.setting.code_rate + 4), 0)\
            ) * self.network.setting.Ts

    def dataToA(self):
        upper = 8 * self.pl

        lower = self.network.setting.sf
        
        if self.network.setting.de:
            lower -= 2 * self.network.setting.de

        lower *= 4

        return upper / lower

    def fi(self):
        upper = 8 * self.pl
        upper -= 4 * self.network.setting.sf
        if self.network.setting.crc:
            upper += 16
        if self.network.setting.nonheader:
            upper -= 20
        upper += 28

        lower = self.network.setting.sf
        
        if self.network.setting.de:
            lower -= 2 * self.network.setting.de

        lower *= 4

        return math.ceil(upper / lower)
    
    